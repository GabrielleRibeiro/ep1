# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

### Votação

<p> Ao iniciar uma nova votação, o usuário informará a quantidade de eleitores que votarão na urna. Será criado um looping de votação, onde em cada iteração do looping será recolhido os dados de cada eleitor (nome, CPF e título de eleitor). </p>
<p> Cada eleitor terá a opção de votar em cada tipo de candidato (presidente, governador, senador, deputado federal e deputado distrital).  
 Quando o eleitor informa o número do candidato que ele deseja votar, é mostrado os dados desse candidato na tela. </p> 
<p> Caso o número informado não corresponda a um candidato da cadastrado, o voto será contabilizado como voto nulo.  
 Além disso, o eleitor tem a opção de votar em branco para cada tipo de candidato. </p>

### Mostrar resultado da votação

<p> Após a votação ser concluída, o usuário pode acessar o resultado da votação, que mostra os vencedores da eleição.</p>  
<p> É informado o candidato que obteve mais votos válidos em cada categoria (presidente, governador, senador, deputado federal e deputado distrital).
 Caso ocorra empate, isso também é informado com os dados dos candidatos empatados. Além disso, também é informado a porcentagem de votos dos candidatos (vencedores ou empatados) e a porcentagem de votos brancos e nulos por categoria.
 Caso não tenha candidato vencedor (todos os votos brancos e/ou nulos) isso é informado ao usuário. </p>
  
<p> Vale ressaltar que a quantidade de votos válidos desconsidera o número de votos brancos e nulos.
 Dessa forma, no cálculo da porcentagem de votos dos candidatos são considerados somente votos válidos (não brancos e não nulos). </p>

### Mostrar relatório de votação

<p> Após a votação o usuário poderá ter acesso ao relatório de votação, que mostra os dados dos eleitores que votaram na urna e em quem cada eleitor votou.  
O dados do relatório de votação podem ser acessados no arquivo Relatorio_de_votacao.txt na pasta Data. </p>
<p> Além disso, o usuário terá a opção de visualizar essas informações na tela. </p>
<p> A cada votação iniciada o relatório de votação é reiniciado, guardando apenas os dados daquela votação em questão. </p>

## Referências

<p> Os dados dos candidatos utilizados nesse projeto correspondem aos dados reais das eleições de 2018 no Brasil.
 Esses dados foram retirados do [Repositório de dados eleitorais](http://www.tse.jus.br/eleicoes/estatisticas/repositorio-de-dados-eleitorais-1/)
do Tribunal Superior Eleitoral. </p> 