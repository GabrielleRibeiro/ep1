#ifndef GOVERNADOR_HPP
#define GOVERNADOR_HPP

#include "candidato.hpp"

class Governador : public Candidato
{

private:
	Candidato * vice_governador;

public:
	Governador();
	~Governador();

	void set_vice_governador(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido);

	void mostrar_dados_do_candidato();
};

#endif
