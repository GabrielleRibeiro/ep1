#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa
{

private:
	string nome;
	string numero_do_cpf;

public:
	Pessoa();
	~Pessoa();

	void set_nome(string nome);
	string get_nome();
	void set_numero_do_cpf(string numero_do_cpf);
	string get_numero_do_cpf();

	void gravar_dados_em_arquivo_de_relatorio_de_votacao();
};

#endif
