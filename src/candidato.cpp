#include "candidato.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Candidato::Candidato()
{
	Pessoa();
	set_nome_da_unidade_eleitoral("");
	set_descricao_do_cargo("");
	set_numero_do_candidato_na_urna("");
	set_nome_de_urna_do_candidato("");
	set_nome_do_partido("");
	set_numero_de_votos(0);
}

Candidato::~Candidato()
{

}

void Candidato::set_nome_da_unidade_eleitoral(string nome_da_unidade_eleitoral)
{
	this-> nome_da_unidade_eleitoral = nome_da_unidade_eleitoral;
}

string Candidato::get_nome_da_unidade_eleitoral()
{
	return nome_da_unidade_eleitoral;
}

void Candidato::set_descricao_do_cargo(string descricao_do_cargo)
{
	this-> descricao_do_cargo = descricao_do_cargo;
}

string Candidato::get_descricao_do_cargo()
{
	return descricao_do_cargo;
}

void Candidato::set_numero_do_candidato_na_urna(string numero_do_candidato_na_urna)
{
	this-> numero_do_candidato_na_urna = numero_do_candidato_na_urna;
}

string Candidato::get_numero_do_candidato_na_urna()
{
	return numero_do_candidato_na_urna;
}

void Candidato::set_nome_de_urna_do_candidato(string nome_de_urna_do_candidato)
{
	this-> nome_de_urna_do_candidato = nome_de_urna_do_candidato;
}

string Candidato::get_nome_de_urna_do_candidato()
{
	return nome_de_urna_do_candidato;
}

void Candidato::set_nome_do_partido(string nome_do_partido)
{
	this-> nome_do_partido = nome_do_partido;
}

string Candidato::get_nome_do_partido()
{
	return nome_do_partido;
}

void Candidato::set_numero_de_votos(int numero_de_votos)
{
	this-> numero_de_votos = numero_de_votos;
}

int Candidato::get_numero_de_votos()
{
	return numero_de_votos;
}

void Candidato::mostrar_dados_do_candidato()
{
	cout << endl << get_descricao_do_cargo() << " - " << get_nome_de_urna_do_candidato() << endl;
	cout << "Nome: " << get_nome() << endl;
	cout << "CPF: " << get_numero_do_cpf() << endl;
	cout << "Nome da Unidade Eleitoral: " << get_nome_da_unidade_eleitoral() << endl;
	cout << "Número do Candidato na Urna: " << get_numero_do_candidato_na_urna() << endl;
	cout << "Nome do Partido: " << get_nome_do_partido() << endl;
}

void Candidato::alterar_dados_do_candidato(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido)
{
	set_nome(nome);
        set_numero_do_cpf(numero_do_cpf);
        set_nome_da_unidade_eleitoral(nome_da_unidade_eleitoral);
        set_descricao_do_cargo(descricao_do_cargo);
       	set_numero_do_candidato_na_urna(numero_do_candidato_na_urna);
        set_nome_de_urna_do_candidato(nome_de_urna_do_candidato);
        set_nome_do_partido(nome_do_partido);
}

void Candidato::adicionar_voto()
{
	this-> numero_de_votos++;
}

void Candidato::gravar_dados_em_arquivo_de_relatorio_de_votacao()
{
	ofstream relatorio_de_votacao;

	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt", ios::app);

	relatorio_de_votacao << endl << get_descricao_do_cargo() << endl;
	relatorio_de_votacao << "Nome: " << get_nome() << endl;
	relatorio_de_votacao << "Nome de Urna: " << get_nome_de_urna_do_candidato() << endl;
	relatorio_de_votacao << "Nome: " << get_nome() << endl;
	relatorio_de_votacao << "CPF: " << get_numero_do_cpf() << endl;
	relatorio_de_votacao << "Nome da Unidade Eleitoral: " << get_nome_da_unidade_eleitoral() << endl;
	relatorio_de_votacao << "Número do Candidato na Urna: " << get_numero_do_candidato_na_urna() << endl;
	relatorio_de_votacao << "Nome do Partido: " << get_nome_do_partido() << endl;

	relatorio_de_votacao.close();
}
