#ifndef URNA_HPP
#define URNA_HPP

#include "candidato.hpp"
#include "presidente.hpp"
#include "governador.hpp"
#include "senador.hpp"
#include "eleitor.hpp"

class Urna
{

private:
	int quantidade_de_eleitores;
	int numero_de_votos_nulos_para_presidente;
	int numero_de_votos_nulos_para_governador;
	int numero_de_votos_nulos_para_senador;
	int numero_de_votos_nulos_para_deputado_federal;
	int numero_de_votos_nulos_para_deputado_distrital;
	int numero_de_votos_em_branco_para_presidente;
	int numero_de_votos_em_branco_para_governador;
	int numero_de_votos_em_branco_para_senador;
	int numero_de_votos_em_branco_para_deputado_federal;
	int numero_de_votos_em_branco_para_deputado_distrital;
	Presidente * candidatos_a_presidencia[13];
	Governador * candidatos_a_governador[11];
	Senador * candidatos_a_senador[20];
	Candidato * candidatos_a_deputado_federal[185];
	Candidato * candidatos_a_deputado_distrital[968];
	Eleitor * eleitor;

public:
	Urna();
	Urna(int quantidade_de_eleitores);
	~Urna();

	void set_quantidade_de_eleitores(int quantidade_de_eleitores);
	int get_quantidade_de_eleitores();

	void set_numero_de_votos_nulos_para_presidente(int numero_de_votos_nulos_para_presidente);
	int get_numero_de_votos_nulos_para_presidente();
	void set_numero_de_votos_nulos_para_governador(int numero_de_votos_nulos_para_governador);
	int get_numero_de_votos_nulos_para_governador();
	void set_numero_de_votos_nulos_para_senador(int numero_de_votos_nulos_para_senador);
	int get_numero_de_votos_nulos_para_senador();
	void set_numero_de_votos_nulos_para_deputado_federal(int numero_de_votos_nulos_para_deputado_federal);
	int get_numero_de_votos_nulos_para_deputado_federal();
	void set_numero_de_votos_nulos_para_deputado_distrital(int numero_de_votos_nulos_para_deputado_distrital);
	int get_numero_de_votos_nulos_para_deputado_distrital();

	void set_numero_de_votos_em_branco_para_presidente(int numero_de_votos_em_branco_para_presidente);
	int get_numero_de_votos_em_branco_para_presidente();
	void set_numero_de_votos_em_branco_para_governador(int numero_de_votos_em_branco_para_governador);
	int get_numero_de_votos_em_branco_para_governador();
	void set_numero_de_votos_em_branco_para_senador(int numero_de_votos_em_branco_para_senador);
	int get_numero_de_votos_em_branco_para_senador();
	void set_numero_de_votos_em_branco_para_deputado_federal(int numero_de_votos_em_branco_para_deputado_federal);
	int get_numero_de_votos_em_branco_para_deputado_federal();
	void set_numero_de_votos_em_branco_para_deputado_distrital(int numero_de_votos_em_branco_para_deputado_distrital);
	int get_numero_de_votos_em_branco_para_deputado_distrital();

	void registrar_candidatos();
	int buscar_candidato(string numero_do_candidato_na_urna, string descricao_do_cargo);
	void coletar_dados_do_eleitor();
	void coletar_voto();
	void looping_de_votacao();
	void apuracao_de_votos();
	void mostrar_relatorio_de_votos();
	void limpar_arquivo_de_relatorio_de_votacao();
};

#endif
