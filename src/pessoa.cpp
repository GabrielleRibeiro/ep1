#include "pessoa.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Pessoa::Pessoa()
{
	set_nome("");
	set_numero_do_cpf("");
}

Pessoa::~Pessoa()
{

}

void Pessoa::set_nome(string nome)
{
	this-> nome = nome;
}

string Pessoa::get_nome()
{
	return nome;
}

void Pessoa::set_numero_do_cpf(string numero_do_cpf)
{
	this-> numero_do_cpf = numero_do_cpf;
}

string Pessoa::get_numero_do_cpf()
{
	return numero_do_cpf;
}

void Pessoa::gravar_dados_em_arquivo_de_relatorio_de_votacao()
{
	ofstream relatorio_de_votacao;

	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt", ios::app);

	relatorio_de_votacao << "Nome: " << get_nome() << endl;
	relatorio_de_votacao << "CPF: " << get_numero_do_cpf() << endl;

	relatorio_de_votacao.close();
}
