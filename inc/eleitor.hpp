#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include "pessoa.hpp"
#include <string>

class Eleitor : public Pessoa
{

private:
	string numero_do_titulo_de_eleitor;

public:
	Eleitor();
	Eleitor(string nome, string numero_do_cpf, string numero_do_titulo_de_eleitor);
	~Eleitor();

	void set_numero_do_titulo_de_eleitor(string numero_do_titulo_de_eleitor);
	string get_numero_do_titulo_de_eleitor();

	void alterar_dados_do_eleitor();
	void gravar_dados_em_arquivo_de_relatorio_de_votacao();
};

#endif
