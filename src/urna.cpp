#include "urna.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

Urna::Urna()
{
	set_quantidade_de_eleitores(0);
	set_numero_de_votos_nulos_para_presidente(0);
	set_numero_de_votos_nulos_para_governador(0);
	set_numero_de_votos_nulos_para_senador(0);
	set_numero_de_votos_nulos_para_deputado_federal(0);
	set_numero_de_votos_nulos_para_deputado_distrital(0);
	set_numero_de_votos_em_branco_para_presidente(0);
	set_numero_de_votos_em_branco_para_governador(0);
	set_numero_de_votos_em_branco_para_senador(0);
	set_numero_de_votos_em_branco_para_deputado_federal(0);
	set_numero_de_votos_em_branco_para_deputado_distrital(0);
	for(int i = 0; i < 13; ++i)
	{
		candidatos_a_presidencia[i] = new Presidente();
	}
	for(int i = 0; i < 11; ++i)
	{
		candidatos_a_governador[i] = new Governador();
	}
	for(int i = 0; i < 20; ++i)
	{
		candidatos_a_senador[i] = new Senador();
	}
	for(int i = 0; i < 185; ++i)
	{
		candidatos_a_deputado_federal[i] = new Candidato();
	}
	for(int i = 0; i < 968; ++i)
	{
		candidatos_a_deputado_distrital[i] = new Candidato();
	}
	eleitor = new Eleitor();
	limpar_arquivo_de_relatorio_de_votacao();
}

Urna::~Urna()
{

}

void Urna::set_quantidade_de_eleitores(int quantidade_de_eleitores)
{
	this-> quantidade_de_eleitores = quantidade_de_eleitores;
}

int Urna::get_quantidade_de_eleitores()
{
	return quantidade_de_eleitores;
}

void Urna::set_numero_de_votos_nulos_para_presidente(int numero_de_votos_nulos_para_presidente)
{
	this-> numero_de_votos_nulos_para_presidente = numero_de_votos_nulos_para_presidente;
}

int Urna::get_numero_de_votos_nulos_para_presidente()
{
	return numero_de_votos_nulos_para_presidente;
}

void Urna::set_numero_de_votos_nulos_para_governador(int numero_de_votos_nulos_para_governador)
{
	this-> numero_de_votos_nulos_para_governador = numero_de_votos_nulos_para_governador;
}

int Urna::get_numero_de_votos_nulos_para_governador()
{
	return numero_de_votos_nulos_para_governador;
}

void Urna::set_numero_de_votos_nulos_para_senador(int numero_de_votos_nulos_para_senador)
{
	this-> numero_de_votos_nulos_para_senador = numero_de_votos_nulos_para_senador;
}

int Urna::get_numero_de_votos_nulos_para_senador()
{
	return numero_de_votos_nulos_para_senador;
}

void Urna::set_numero_de_votos_nulos_para_deputado_federal(int numero_de_votos_nulos_para_deputado_federal)
{
	this-> numero_de_votos_nulos_para_deputado_federal = numero_de_votos_nulos_para_deputado_federal;
}

int Urna::get_numero_de_votos_nulos_para_deputado_federal()
{
	return numero_de_votos_nulos_para_deputado_federal;
}

void Urna::set_numero_de_votos_nulos_para_deputado_distrital(int numero_de_votos_nulos_para_deputado_distrital)
{
	this-> numero_de_votos_nulos_para_deputado_distrital = numero_de_votos_nulos_para_deputado_distrital;
}

int Urna::get_numero_de_votos_nulos_para_deputado_distrital()
{
	return numero_de_votos_nulos_para_deputado_distrital;
}

void Urna::set_numero_de_votos_em_branco_para_presidente(int numero_de_votos_em_branco_para_presidente)
{
	this-> numero_de_votos_em_branco_para_presidente = numero_de_votos_em_branco_para_presidente;
}

int Urna::get_numero_de_votos_em_branco_para_presidente()
{
	return numero_de_votos_em_branco_para_presidente;
}

void Urna::set_numero_de_votos_em_branco_para_governador(int numero_de_votos_em_branco_para_governador)
{
	this-> numero_de_votos_em_branco_para_governador = numero_de_votos_em_branco_para_governador;
}

int Urna::get_numero_de_votos_em_branco_para_governador()
{
	return numero_de_votos_em_branco_para_governador;
}

void Urna::set_numero_de_votos_em_branco_para_senador(int numero_de_votos_em_branco_para_senador)
{
	this-> numero_de_votos_em_branco_para_senador = numero_de_votos_em_branco_para_senador;
}

int Urna::get_numero_de_votos_em_branco_para_senador()
{
	return numero_de_votos_em_branco_para_senador;
}

void Urna::set_numero_de_votos_em_branco_para_deputado_federal(int numero_de_votos_em_branco_para_deputado_federal)
{
	this-> numero_de_votos_em_branco_para_deputado_federal = numero_de_votos_em_branco_para_deputado_federal;
}

int Urna::get_numero_de_votos_em_branco_para_deputado_federal()
{
	return numero_de_votos_em_branco_para_deputado_federal;
}

void Urna::set_numero_de_votos_em_branco_para_deputado_distrital(int numero_de_votos_em_branco_para_deputado_distrital)
{
	this-> numero_de_votos_em_branco_para_deputado_distrital = numero_de_votos_em_branco_para_deputado_distrital;
}

int Urna::get_numero_de_votos_em_branco_para_deputado_distrital()
{
	return numero_de_votos_em_branco_para_deputado_distrital;
}

void Urna::registrar_candidatos()
{
	ifstream arquivo_de_dados_dos_candidatos;
	string linha;
	int contador_de_ponto_e_virgula;

	string nome;
	string numero_do_cpf;
	string nome_da_unidade_eleitoral;
	string descricao_do_cargo;
	string numero_do_candidato_na_urna;
	string nome_de_urna_do_candidato;
	string nome_do_partido;
	
	int incremento_dos_candidatos_a_presidencia = 0;
	int incremento_dos_candidatos_a_governador = 0;
	int incremento_dos_candidatos_a_senador = 0;
	int incremento_dos_candidatos_a_deputado_federal = 0;
	int incremento_dos_candidatos_a_deputado_distrital = 0;

	arquivo_de_dados_dos_candidatos.open("./data/consulta_cand_2018_BR.csv");

	if (!arquivo_de_dados_dos_candidatos)
	{
		 cout << "Não foi possível abrir o arquivo\n";
	}

	// Registro de candidatos a presidencia
	while(getline(arquivo_de_dados_dos_candidatos, linha))
	{
		contador_de_ponto_e_virgula = 0;

		nome.clear();
		numero_do_cpf.clear();
		nome_da_unidade_eleitoral.clear();
		descricao_do_cargo.clear();
		numero_do_candidato_na_urna.clear();
		nome_de_urna_do_candidato.clear();
		nome_do_partido.clear();

		for(int i = 0; i < linha.size(); ++i)
		{
			if(linha[i] == ';')
			{
	                        contador_de_ponto_e_virgula++;
	                }
			else if(contador_de_ponto_e_virgula == 12)
			{
				if(linha[i] != '"')
				{
					nome_da_unidade_eleitoral.push_back(linha[i]);
				}
			}
			else if(contador_de_ponto_e_virgula == 14)
			{
				if(linha[i] != '"')
				{
	                                descricao_do_cargo.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 16)
			{
				if(linha[i] != '"')
				{
	                                 numero_do_candidato_na_urna.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 17)
			{
	                        if(linha[i] != '"')
				{
	                                 nome.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 18)
			{
	                        if(linha[i] != '"')
				{
	                                 nome_de_urna_do_candidato.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 20)
			{
	                        if(linha[i] != '"')
				{
	                                 numero_do_cpf.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 29)
			{
	                        if(linha[i] != '"')
				{
	                                  nome_do_partido.push_back(linha[i]);
	                        }
	                }
		}

		if(descricao_do_cargo == "PRESIDENTE")
		{
			candidatos_a_presidencia[incremento_dos_candidatos_a_presidencia]-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);	

			incremento_dos_candidatos_a_presidencia++;
		} 		
	}

	// Registro de candidatos a vice-presidencia
	arquivo_de_dados_dos_candidatos.close();
	arquivo_de_dados_dos_candidatos.open("./data/consulta_cand_2018_BR.csv");

	while(getline(arquivo_de_dados_dos_candidatos, linha))
	{
		contador_de_ponto_e_virgula = 0;

		nome.clear();
		numero_do_cpf.clear();
		nome_da_unidade_eleitoral.clear();
		descricao_do_cargo.clear();
		numero_do_candidato_na_urna.clear();
		nome_de_urna_do_candidato.clear();
		nome_do_partido.clear();

		for(int i = 0; i < linha.size(); ++i)
		{
			if(linha[i] == ';')
			{
	                        contador_de_ponto_e_virgula++;
	                }
			else if(contador_de_ponto_e_virgula == 12)
			{
				if(linha[i] != '"')
				{
					nome_da_unidade_eleitoral.push_back(linha[i]);
				}
			}
			else if(contador_de_ponto_e_virgula == 14)
			{
				if(linha[i] != '"')
				{
	                                descricao_do_cargo.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 16)
			{
				if(linha[i] != '"')
				{
	                                 numero_do_candidato_na_urna.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 17)
			{
	                        if(linha[i] != '"')
				{
	                                 nome.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 18)
			{
	                        if(linha[i] != '"')
				{
	                                 nome_de_urna_do_candidato.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 20)
			{
	                        if(linha[i] != '"')
				{
	                                 numero_do_cpf.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 29)
			{
	                        if(linha[i] != '"')
				{
	                                  nome_do_partido.push_back(linha[i]);
	                        }
	                }
		}

		if(descricao_do_cargo == "VICE-PRESIDENTE")
		{
			for(int i = 0; i < 13; ++i)
			{
				if(candidatos_a_presidencia[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
				{
					candidatos_a_presidencia[i]-> set_vice_presidente(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
					break;
				}
			}			
		} 				
	}

	// Registro de candidatos a governador, senador e deputados federal e distrital	
	arquivo_de_dados_dos_candidatos.close();
	arquivo_de_dados_dos_candidatos.open("./data/consulta_cand_2018_DF.csv");

	while(getline(arquivo_de_dados_dos_candidatos, linha))
	{
		contador_de_ponto_e_virgula = 0;

		nome.clear();
		numero_do_cpf.clear();
		nome_da_unidade_eleitoral.clear();
		descricao_do_cargo.clear();
		numero_do_candidato_na_urna.clear();
		nome_de_urna_do_candidato.clear();
		nome_do_partido.clear();

		for(int i = 0; i < linha.size(); ++i)
		{
			if(linha[i] == ';')
			{
	                        contador_de_ponto_e_virgula++;
	                }
			else if(contador_de_ponto_e_virgula == 12)
			{
				if(linha[i] != '"')
				{
					nome_da_unidade_eleitoral.push_back(linha[i]);
				}
			}
			else if(contador_de_ponto_e_virgula == 14)
			{
				if(linha[i] != '"')
				{
	                                descricao_do_cargo.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 16)
			{
				if(linha[i] != '"')
				{
	                                 numero_do_candidato_na_urna.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 17)
			{
	                        if(linha[i] != '"')
				{
	                                 nome.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 18)
			{
	                        if(linha[i] != '"')
				{
	                                 nome_de_urna_do_candidato.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 20)
			{
	                        if(linha[i] != '"')
				{
	                                 numero_do_cpf.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 29)
			{
	                        if(linha[i] != '"')
				{
	                                  nome_do_partido.push_back(linha[i]);
	                        }
	                }
		}

		if(descricao_do_cargo == "GOVERNADOR")
		{
			candidatos_a_governador[incremento_dos_candidatos_a_governador]-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);

			incremento_dos_candidatos_a_governador++;
		}
		else if(descricao_do_cargo == "SENADOR")
		{
			candidatos_a_senador[incremento_dos_candidatos_a_senador]-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);

			incremento_dos_candidatos_a_senador++;
		}
		else if(descricao_do_cargo == "DEPUTADO FEDERAL")
		{
			candidatos_a_deputado_federal[incremento_dos_candidatos_a_deputado_federal]-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);

			incremento_dos_candidatos_a_deputado_federal++;
		}
		else if(descricao_do_cargo == "DEPUTADO DISTRITAL")
		{
			candidatos_a_deputado_distrital[incremento_dos_candidatos_a_deputado_distrital]-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);

			incremento_dos_candidatos_a_deputado_distrital++;
		}		
	}
	
	// Registro de candidatos a vice_governador e suplentes
	arquivo_de_dados_dos_candidatos.close();
	arquivo_de_dados_dos_candidatos.open("./data/consulta_cand_2018_DF.csv");

	while(getline(arquivo_de_dados_dos_candidatos, linha))
	{
		contador_de_ponto_e_virgula = 0;

		nome.clear();
		numero_do_cpf.clear();
		nome_da_unidade_eleitoral.clear();
		descricao_do_cargo.clear();
		numero_do_candidato_na_urna.clear();
		nome_de_urna_do_candidato.clear();
		nome_do_partido.clear();

		for(int i = 0; i < linha.size(); ++i)
		{
			if(linha[i] == ';')
			{
	                        contador_de_ponto_e_virgula++;
	                }
			else if(contador_de_ponto_e_virgula == 12)
			{
				if(linha[i] != '"')
				{
					nome_da_unidade_eleitoral.push_back(linha[i]);
				}
			}
			else if(contador_de_ponto_e_virgula == 14)
			{
				if(linha[i] != '"')
				{
	                                descricao_do_cargo.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 16)
			{
				if(linha[i] != '"')
				{
	                                 numero_do_candidato_na_urna.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 17)
			{
	                        if(linha[i] != '"')
				{
	                                 nome.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 18)
			{
	                        if(linha[i] != '"')
				{
	                                 nome_de_urna_do_candidato.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 20)
			{
	                        if(linha[i] != '"')
				{
	                                 numero_do_cpf.push_back(linha[i]);
	                        }
	                }
			else if(contador_de_ponto_e_virgula == 29)
			{
	                        if(linha[i] != '"')
				{
	                                  nome_do_partido.push_back(linha[i]);
	                        }
	                }
		} 

		if(descricao_do_cargo == "VICE-GOVERNADOR")
		{
			for(int i = 0; i < 11; ++i)
			{
				if(candidatos_a_governador[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
				{
					candidatos_a_governador[i]-> set_vice_governador(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
				}
			}			
		}
		else if(descricao_do_cargo == "PRIMEIRO SUPLENTE")
		{
			for(int i = 0; i < 20; ++i)
			{
				if(candidatos_a_senador[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
				{
					candidatos_a_senador[i]-> set_primeiro_suplente(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
				}
			}	
		}	
		else if(descricao_do_cargo == "SEGUNDO SUPLENTE")
		{
			for(int i = 0; i < 20; ++i)
			{
				if(candidatos_a_senador[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
				{
					candidatos_a_senador[i]-> set_segundo_suplente(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
				}
			}	
		}				
	}
}

int Urna::buscar_candidato(string numero_do_candidato_na_urna, string descricao_do_cargo)
{
	int indice_do_candidato_no_vetor = -1;

	if(descricao_do_cargo == "DEPUTADO DISTRITAL")
	{
		for(int i = 0; i < 968; ++i)
		{
			if(candidatos_a_deputado_distrital[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
			{
				indice_do_candidato_no_vetor = i;
				break;
			}
		}

	}
	else if(descricao_do_cargo == "DEPUTADO FEDERAL")
	{
		for(int i = 0; i < 185; ++i)
		{
			if(candidatos_a_deputado_federal[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)		
			{
				indice_do_candidato_no_vetor = i;
				break;
			}
		}
	}
	else if(descricao_do_cargo == "SENADOR")
	{
		for(int i = 0; i < 20; ++i)
		{
			if(candidatos_a_senador[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
			{
				indice_do_candidato_no_vetor = i;
				break;
			}
		}	
	}
	else if(descricao_do_cargo == "GOVERNADOR")
	{
		for(int i = 0; i < 11; ++i)
		{
			if(candidatos_a_governador[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
			{
				indice_do_candidato_no_vetor = i;
				break;
			}
		}
	}
	else if(descricao_do_cargo == "PRESIDENTE")
	{
		for(int i = 0; i < 13; ++i)
		{
			if(candidatos_a_presidencia[i]-> get_numero_do_candidato_na_urna() == numero_do_candidato_na_urna)
			{		
				indice_do_candidato_no_vetor = i;
				break;
			}
		}
	}
	return indice_do_candidato_no_vetor;
}

void Urna::coletar_dados_do_eleitor()
{
	int opcao;	

	do
	{	system("clear");
		eleitor-> alterar_dados_do_eleitor();
		cout << "\nConfirmar dados do eleitor\n";
		cout << "1 - Confirma\n";
		cout << "2 - Cancela\n";
		cout << "Informe o número da opção desejada: ";
		cin >> opcao;
	
	}while(opcao != 1);
}

void Urna::coletar_voto()
{
	bool modo_votacao = true;
	int candidato_a_ser_votado = 1;
	int opcao_de_voto;
	int opcao;
	int posicao_do_candidato_no_vetor;
	string codigo_do_candidato;
	ofstream relatorio_de_votacao;

	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt", ios::app);

	cout << "\nELEIÇÕES 2018 - COLETA DE VOTOS\n";
	
	coletar_dados_do_eleitor();

	eleitor-> gravar_dados_em_arquivo_de_relatorio_de_votacao();

	relatorio_de_votacao << endl << "VOTOS DO ELEITOR" << endl;

	do
	{
		switch(candidato_a_ser_votado)
		{
			case 1:
				system("clear");

				cout << "\nVOTO PARA DEPUTADO DISTRITAL\n";
				cout << "Opções:\n";
				cout << "1 - Votar em candidato a deputado distrital\n";
				cout << "2 - Voto em branco para deputado distrital\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_de_voto;

				if(opcao_de_voto == 1)
				{
					cout << "\nInforme o código do candidato a deputado distrital: ";
					cin >> codigo_do_candidato;

					posicao_do_candidato_no_vetor = buscar_candidato(codigo_do_candidato, "DEPUTADO DISTRITAL");
					
					if(posicao_do_candidato_no_vetor != -1)
					{
						candidatos_a_deputado_distrital[posicao_do_candidato_no_vetor]-> mostrar_dados_do_candidato();
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							candidato_a_ser_votado++;
							candidatos_a_deputado_distrital[posicao_do_candidato_no_vetor]-> gravar_dados_em_arquivo_de_relatorio_de_votacao();
							candidatos_a_deputado_distrital[posicao_do_candidato_no_vetor]-> adicionar_voto();
						}
					}
					else
					{
						cout << "\nO código informado não corresponde a um candidato válido!\n";
						cout << "Deseja mesmo votar nulo para deputado distrital?\n";
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							this-> numero_de_votos_nulos_para_deputado_distrital++;
							candidato_a_ser_votado++;
							relatorio_de_votacao << endl << "DEPUTADO DISTRITAL" << endl << "Voto nulo" << endl;
						}
					}

				}
				else if(opcao_de_voto == 2)
				{
					cout << "\nDeseja mesmo votar em branco para deputado distrital?\n";
					cout << "1 - Confirma\n";
					cout << "2 - Cancela\n";
					cout << "Informe o número da opção escolhida: ";
					cin >> opcao;

					if(opcao == 1)
					{
						this-> numero_de_votos_em_branco_para_deputado_distrital++;
						candidato_a_ser_votado++;
						relatorio_de_votacao << endl << "DEPUTADO DISTRITAL" << endl << "Voto em branco" << endl;
					}

				}
				break;

			case 2:
				system("clear");

				cout << "\nVOTO PARA DEPUTADO FEDERAL\n";
				cout << "Opções:\n";
				cout << "1 - Votar em candidato a deputado federal\n";
				cout << "2 - Voto em branco para deputado federal\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_de_voto;

				if(opcao_de_voto == 1)
				{
					cout << "\nInforme o código do candidato a deputado federal: ";
					cin >> codigo_do_candidato;

					posicao_do_candidato_no_vetor = buscar_candidato(codigo_do_candidato, "DEPUTADO FEDERAL");

					if(posicao_do_candidato_no_vetor != -1)
					{
						candidatos_a_deputado_federal[posicao_do_candidato_no_vetor]-> mostrar_dados_do_candidato();
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							candidato_a_ser_votado++;
							candidatos_a_deputado_federal[posicao_do_candidato_no_vetor]-> gravar_dados_em_arquivo_de_relatorio_de_votacao();
							candidatos_a_deputado_federal[posicao_do_candidato_no_vetor]-> adicionar_voto();
						}
					}
					else
					{
						cout << "\nO código informado não corresponde a um candidato válido!\n";
						cout << "Deseja mesmo votar nulo para deputado federal?\n";
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							this-> numero_de_votos_nulos_para_deputado_federal++;
							candidato_a_ser_votado++;
							relatorio_de_votacao << endl << "DEPUTADO FEDERAL" << endl << "Voto nulo" << endl;
						}
					}
				}
				else if(opcao_de_voto == 2)
				{
					cout << "\nDeseja mesmo votar em branco para deputado federal?\n";
					cout << "1 - Confirma\n";
					cout << "2 - Cancela\n";
					cout << "Informe o número da opção escolhida: ";
					cin >> opcao;

					if(opcao == 1)
					{
						this-> numero_de_votos_em_branco_para_deputado_federal++;
						candidato_a_ser_votado++;
						relatorio_de_votacao << endl << "DEPUTADO FEDERAL" << endl << "Voto em branco" << endl;
					}
				}
				break;

			case 3:
				system("clear");

				cout << "\nVOTO PARA SENADOR\n";
				cout << "Opções:\n";
				cout << "1 - Votar em candidato a senador\n";
				cout << "2 - Voto em branco para senador\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_de_voto;

				if(opcao_de_voto == 1)
				{
					cout << "\nInforme o código do candidato a senador: ";
					cin >> codigo_do_candidato;

					posicao_do_candidato_no_vetor = buscar_candidato(codigo_do_candidato, "SENADOR");

					if(posicao_do_candidato_no_vetor != -1)
					{
						candidatos_a_senador[posicao_do_candidato_no_vetor]-> mostrar_dados_do_candidato();
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							candidato_a_ser_votado++;
							candidatos_a_senador[posicao_do_candidato_no_vetor]-> gravar_dados_em_arquivo_de_relatorio_de_votacao();
							candidatos_a_senador[posicao_do_candidato_no_vetor]-> adicionar_voto();
						}
					}
					else
					{
						cout << "\nO código informado não corresponde a um candidato válido!\n";
						cout << "Deseja mesmo votar nulo para senador?\n";
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							this-> numero_de_votos_nulos_para_senador++;
							candidato_a_ser_votado++;
							relatorio_de_votacao << endl << "SENADOR" << endl << "Voto nulo" << endl;
						}
					}
				}
				else if(opcao_de_voto == 2)
				{
					cout << "\nDeseja mesmo votar em branco para senador?\n";
					cout << "1 - Confirma\n";
					cout << "2 - Cancela\n";
					cout << "Informe o número da opção escolhida: ";
					cin >> opcao;

					if(opcao == 1)
					{
						this-> numero_de_votos_em_branco_para_senador++;
						candidato_a_ser_votado++;
						relatorio_de_votacao << endl << "SENADOR" << endl << "Voto em branco" << endl;
					}

				}
				break;

			case 4:
				system("clear");

				cout << "\nVOTO PARA GOVERNADOR\n";
				cout << "Opções:\n";
				cout << "1 - Votar em candidato a governador\n";
				cout << "2 - Voto em branco para governador\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_de_voto;

				if(opcao_de_voto == 1)
				{
					cout << "\nInforme o código do candidato a governador: ";
					cin >> codigo_do_candidato;

					posicao_do_candidato_no_vetor = buscar_candidato(codigo_do_candidato, "GOVERNADOR");

					if(posicao_do_candidato_no_vetor != -1)
					{
						candidatos_a_governador[posicao_do_candidato_no_vetor]-> mostrar_dados_do_candidato();
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							candidato_a_ser_votado++;
							candidatos_a_governador[posicao_do_candidato_no_vetor]-> gravar_dados_em_arquivo_de_relatorio_de_votacao();
							candidatos_a_governador[posicao_do_candidato_no_vetor]-> adicionar_voto();
						}
					}
					else
					{
						cout << "\nO código informado não corresponde a um candidato válido!\n";
						cout << "Deseja mesmo votar nulo para governador?\n";
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							this-> numero_de_votos_nulos_para_governador++;
							candidato_a_ser_votado++;
							relatorio_de_votacao << endl << "GOVERNADOR" << endl << "Voto nulo" << endl;
						}
					}
				}
				else if(opcao_de_voto == 2)
				{
					cout << "\nDeseja mesmo votar em branco para governador?\n";
					cout << "1 - Confirma\n";
					cout << "2 - Cancela\n";
					cout << "Informe o número da opção escolhida: ";
					cin >> opcao;

					if(opcao == 1)
					{
						this-> numero_de_votos_em_branco_para_governador++;
						candidato_a_ser_votado++;
						relatorio_de_votacao << endl << "GOVERNADOR" << endl << "Voto em branco" << endl;
					}

				}
				break;

			case 5:
				system("clear");

				cout << "\nVOTO PARA PRESIDENTE\n";
				cout << "Opções:\n";
				cout << "1 - Votar em candidato a presidente\n";
				cout << "2 - Voto em branco para presidente\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_de_voto;

				if(opcao_de_voto == 1)
				{
					cout << "\nInforme o código do candidato a presidente: ";
					cin >> codigo_do_candidato;

					posicao_do_candidato_no_vetor = buscar_candidato(codigo_do_candidato, "PRESIDENTE");

					if(posicao_do_candidato_no_vetor != -1)
					{
						candidatos_a_presidencia[posicao_do_candidato_no_vetor]-> mostrar_dados_do_candidato();
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							modo_votacao = false;
							candidatos_a_presidencia[posicao_do_candidato_no_vetor]-> gravar_dados_em_arquivo_de_relatorio_de_votacao();
							candidatos_a_presidencia[posicao_do_candidato_no_vetor]-> adicionar_voto();
							relatorio_de_votacao << "----------------------------------------------------------------";
							relatorio_de_votacao << endl;
						}
					}
					else
					{
						cout << "\nO código informado não corresponde a um candidato válido!\n";
						cout << "Deseja mesmo votar nulo para presidente?\n";
						cout << "\nOpções:\n";
						cout << "1 - Confirma\n";
						cout << "2 - Cancela\n";
						cout << "Informe o número da opção escolhida: ";
						cin >> opcao;

						if(opcao == 1)
						{
							modo_votacao = false;
							this-> numero_de_votos_nulos_para_presidente++;
							candidato_a_ser_votado++;
							relatorio_de_votacao << endl << "PRESIDENTE" << endl << "Voto nulo" << endl;
							relatorio_de_votacao << "----------------------------------------------------------------";
							relatorio_de_votacao << endl;
						}
					}
				}
				else if(opcao_de_voto == 2)
				{
					cout << "\nDeseja mesmo votar em branco para presidente?\n";
					cout << "1 - Confirma\n";
					cout << "2 - Cancela\n";
					cout << "Informe o número da opção escolhida: ";
					cin >> opcao;

					if(opcao == 1)
					{
						modo_votacao = false;
						this-> numero_de_votos_em_branco_para_presidente++;
						relatorio_de_votacao << endl << "PRESIDENTE" << endl << "Voto em branco" << endl;
						relatorio_de_votacao << "----------------------------------------------------------------";
						relatorio_de_votacao << endl;
					}
				}
				break;
		}	
	}while(modo_votacao);
}

void Urna::looping_de_votacao()
{
	for(int i = 0; i < this-> quantidade_de_eleitores; ++i)
	{
		coletar_voto();
	}
}

void Urna::apuracao_de_votos()
{
	int maior_quantidade_de_votos = -1;
	int indice_do_candidato_com_maior_quantidade_de_votos;
	int contador_da_maior_quantidade_de_votos = 0;
	bool empate = false;
	vector <int> indice_dos_candidatos_empatados;

	indice_dos_candidatos_empatados.clear();

	cout << "\n----------------------------------------------------------------\n";

	for(int i = 0; i < 13; ++i)
	{
		if(candidatos_a_presidencia[i]-> get_numero_de_votos() > maior_quantidade_de_votos)
		{
			maior_quantidade_de_votos = candidatos_a_presidencia[i]-> get_numero_de_votos();
			indice_do_candidato_com_maior_quantidade_de_votos = i;	
		}
	}
	if(maior_quantidade_de_votos == 0)
	{
		cout << "\nNÃO HOUVE CANDIDATO A PRESIDÊNCIA VENCEDOR!\n";
		cout << "Nenhum candidato teve votos válidos!\n";
	}
	else
	{
		for(int i = 0; i < 13; ++i)
		{
			if(candidatos_a_presidencia[i]-> get_numero_de_votos() == maior_quantidade_de_votos)
			{
				indice_dos_candidatos_empatados.push_back(i);
				contador_da_maior_quantidade_de_votos++;
			}
		}
		if(contador_da_maior_quantidade_de_votos > 1)
		{
			empate = true;
		}
		if(empate == false)
		{
			cout << "\nCANDIDATO A PRESIDENCIA VENCEDOR" << endl;
			candidatos_a_presidencia[indice_do_candidato_com_maior_quantidade_de_votos]-> mostrar_dados_do_candidato();
			cout << "\nQuantidade de votos: ";
			cout << candidatos_a_presidencia[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() << endl;
			cout << "Porcentagem de votos: "; 
			cout << candidatos_a_presidencia[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_em_branco_para_presidente - this-> numero_de_votos_nulos_para_presidente) << "%" << endl;
		}
		else
		{
			cout << "\nEMPATE ENTRE CANDIDATOS A PRESIDENCIA\n";
			for(int i = 1, cont = 0; i <= indice_dos_candidatos_empatados.size(); ++i, ++cont)
			{
				cout << "\nCANDIDATO " << i << endl;
				candidatos_a_presidencia[indice_dos_candidatos_empatados[cont]]-> mostrar_dados_do_candidato();
				cout << "\nQuantidade de votos: ";
				cout << candidatos_a_presidencia[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() << endl;
				cout << "Porcentagem de votos: "; 
				cout << candidatos_a_presidencia[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_em_branco_para_presidente - this-> numero_de_votos_nulos_para_presidente) << "%" << endl;
			}
		}
	}
	cout << "\nPORCENTAGEM DE VOTOS NULOS E BRANCOS PARA PRESIDENTE\n";
	cout << "Votos em branco: " << (this-> numero_de_votos_em_branco_para_presidente * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "Votos nulos: " << (this-> numero_de_votos_nulos_para_presidente * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "\n----------------------------------------------------------------\n";
	maior_quantidade_de_votos = -1;
	empate = false;
	indice_dos_candidatos_empatados.clear();
	contador_da_maior_quantidade_de_votos = 0;
	
	for(int i = 0; i < 11; ++i)
	{	
		if(candidatos_a_governador[i]-> get_numero_de_votos() > maior_quantidade_de_votos)
		{
			maior_quantidade_de_votos = candidatos_a_governador[i]-> get_numero_de_votos();
			indice_do_candidato_com_maior_quantidade_de_votos = i;	
		}
	}
	if(maior_quantidade_de_votos == 0)
	{
		cout << "\nNÃO HOUVE CANDIDATO A GOVERNADOR VENCEDOR!\n";
		cout << "Nenhum candidato teve votos válidos!\n";

	}
	else
	{
		for(int i = 0; i < 11; ++i)
		{	
			if(candidatos_a_governador[i]-> get_numero_de_votos() == maior_quantidade_de_votos)
			{
				indice_dos_candidatos_empatados.push_back(i);
				contador_da_maior_quantidade_de_votos++;
			}
		}
		if(contador_da_maior_quantidade_de_votos > 1)
		{
			empate = true;
		}
		if(empate == false)
		{
			cout << "\nCANDIDATO A GOVERNADOR VENCEDOR" << endl;
			candidatos_a_governador[indice_do_candidato_com_maior_quantidade_de_votos]-> mostrar_dados_do_candidato();
			cout << "\nQuantidade de votos: ";
			cout << candidatos_a_governador[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() << endl;
			cout << "Porcentagem de votos: "; 
			cout << candidatos_a_governador[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_governador - this-> numero_de_votos_em_branco_para_governador) << "%" << endl;
		}
		else
		{
			cout << "\nEMPATE ENTRE CANDIDATOS A GOVERNADOR\n";
			for(int i = 1, cont = 0; i <= indice_dos_candidatos_empatados.size(); ++i, ++cont)
			{
				cout << "\nCANDIDATO " << i << endl;
				candidatos_a_governador[indice_dos_candidatos_empatados[cont]]-> mostrar_dados_do_candidato();
				cout << "\nQuantidade de votos: ";
				cout << candidatos_a_governador[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() << endl;
				cout << "Porcentagem de votos: "; 
				cout << candidatos_a_governador[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_governador - this-> numero_de_votos_em_branco_para_governador) << "%" << endl;
			}
		}
	}
	cout << "\nPORCENTAGEM DE VOTOS NULOS E BRANCOS PARA GOVERNADOR\n";
	cout << "Votos em branco: " << (this-> numero_de_votos_em_branco_para_governador * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "Votos nulos: " << (this-> numero_de_votos_nulos_para_governador * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "\n----------------------------------------------------------------\n";

	maior_quantidade_de_votos = -1;
	empate = false;
	indice_dos_candidatos_empatados.clear();
	contador_da_maior_quantidade_de_votos = 0;
	
	for(int i = 0; i < 20; ++i)
	{	
		if(candidatos_a_senador[i]-> get_numero_de_votos() > maior_quantidade_de_votos)
		{
			maior_quantidade_de_votos = candidatos_a_senador[i]-> get_numero_de_votos();
			indice_do_candidato_com_maior_quantidade_de_votos = i;	
		}
	}
	if(maior_quantidade_de_votos == 0)
	{
		cout << "\nNÃO HOUVE CANDIDATO A SENADOR VENCEDOR!\n";
		cout << "Nenhum candidato teve votos válidos!\n";

	}
	else
	{
		for(int i = 0; i < 20; ++i)
		{	
			if(candidatos_a_senador[i]-> get_numero_de_votos() == maior_quantidade_de_votos)
			{
				indice_dos_candidatos_empatados.push_back(i);
				contador_da_maior_quantidade_de_votos++;
			}
		}
		if(contador_da_maior_quantidade_de_votos > 1)
		{
			empate = true;
		}
		if(empate == false)
		{
			cout << "\nCANDIDATO A SENADOR VENCEDOR" << endl;
			candidatos_a_senador[indice_do_candidato_com_maior_quantidade_de_votos]-> mostrar_dados_do_candidato();
			cout << "\nQuantidade de votos: ";
			cout << candidatos_a_senador[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() << endl;
			cout << "Porcentagem de votos: "; 
			cout << candidatos_a_senador[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_senador - this-> numero_de_votos_em_branco_para_senador) << "%" << endl;
		}
		else
		{
			cout << "\nEMPATE ENTRE CANDIDATOS A SENADOR\n";
			for(int i = 1, cont = 0; i <= indice_dos_candidatos_empatados.size(); ++i, ++cont)
			{
				cout << "\nCANDIDATO " << i << endl;
				candidatos_a_senador[indice_dos_candidatos_empatados[cont]]-> mostrar_dados_do_candidato();
				cout << "\nQuantidade de votos: ";
				cout << candidatos_a_senador[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() << endl;
				cout << "Porcentagem de votos: "; 
				cout << candidatos_a_senador[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_senador - this-> numero_de_votos_em_branco_para_senador) << "%" << endl;
			}
		}
	}
	cout << "\nPORCENTAGEM DE VOTOS NULOS E BRANCOS PARA SENADOR\n";
	cout << "Votos em branco: " << (this-> numero_de_votos_em_branco_para_senador * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "Votos nulos: " << (this-> numero_de_votos_nulos_para_senador * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "\n----------------------------------------------------------------\n";

	maior_quantidade_de_votos = -1;
	empate = false;
	indice_dos_candidatos_empatados.clear();
	contador_da_maior_quantidade_de_votos = 0;
	
	for(int i = 0; i < 185; ++i)
	{	
		if(candidatos_a_deputado_federal[i]-> get_numero_de_votos() > maior_quantidade_de_votos)
		{
			maior_quantidade_de_votos = candidatos_a_deputado_federal[i]-> get_numero_de_votos();
			indice_do_candidato_com_maior_quantidade_de_votos = i;	
		}
	}
	if(maior_quantidade_de_votos == 0)
	{
		cout << "\nNÃO HOUVE CANDIDATO A DEPUTADO FEDERAL VENCEDOR!\n";
		cout << "Nenhum candidato teve votos válidos!\n";
	}
	else
	{
		for(int i = 0; i < 185; ++i)
		{	
			if(candidatos_a_deputado_federal[i]-> get_numero_de_votos() == maior_quantidade_de_votos)
			{
				indice_dos_candidatos_empatados.push_back(i);
				contador_da_maior_quantidade_de_votos++;
			}
		}
		if(contador_da_maior_quantidade_de_votos > 1)
		{
			empate = true;
		}
		if(empate == false)
		{
			cout << "\nCANDIDATO A DEPUTADO FEDERAL VENCEDOR" << endl;
			candidatos_a_deputado_federal[indice_do_candidato_com_maior_quantidade_de_votos]-> mostrar_dados_do_candidato();
			cout << "\nQuantidade de votos: ";
			cout << candidatos_a_deputado_federal[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() << endl;
			cout << "Porcentagem de votos: "; 
			cout << candidatos_a_deputado_federal[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_deputado_federal - this-> numero_de_votos_em_branco_para_deputado_federal) << "%" << endl;
		}
		else
		{
			cout << "\nEMPATE ENTRE CANDIDATOS A DEPUTADO FEDERAL\n";
			for(int i = 1, cont = 0; i <= indice_dos_candidatos_empatados.size(); ++i, ++cont)
			{
				cout << "\nCANDIDATO " << i << endl;
				candidatos_a_deputado_federal[indice_dos_candidatos_empatados[cont]]-> mostrar_dados_do_candidato();
				cout << "\nQuantidade de votos: ";
				cout << candidatos_a_deputado_federal[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() << endl;
				cout << "Porcentagem de votos: "; 
				cout << candidatos_a_deputado_federal[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_deputado_federal - this-> numero_de_votos_em_branco_para_deputado_federal) << "%" << endl;
			}
		}
	}
	cout << "\nPORCENTAGEM DE VOTOS NULOS E BRANCOS PARA DEPUTADO FEDERAL\n";
	cout << "Votos em branco: " << (this-> numero_de_votos_em_branco_para_deputado_federal * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "Votos nulos: " << (this-> numero_de_votos_nulos_para_deputado_federal * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "\n----------------------------------------------------------------\n";

	maior_quantidade_de_votos = -1;
	empate = false;
	indice_dos_candidatos_empatados.clear();
	contador_da_maior_quantidade_de_votos = 0;
	
	for(int i = 0; i < 968; ++i)
	{	
		if(candidatos_a_deputado_distrital[i]-> get_numero_de_votos() > maior_quantidade_de_votos)
		{
			maior_quantidade_de_votos = candidatos_a_deputado_distrital[i]-> get_numero_de_votos();
			indice_do_candidato_com_maior_quantidade_de_votos = i;	
		}
	}
	if(maior_quantidade_de_votos == 0)
	{
		cout << "\nNÃO HOUVE CANDIDATO A DEPUTADO DISTRITAL VENCEDOR!\n";
		cout << "Nenhum candidato teve votos válidos!\n";

	}
	else
	{
		for(int i = 0; i < 968; ++i)
		{	
			if(candidatos_a_deputado_distrital[i]-> get_numero_de_votos() == maior_quantidade_de_votos)
			{
				indice_dos_candidatos_empatados.push_back(i);
				contador_da_maior_quantidade_de_votos++;
			}
		}
		if(contador_da_maior_quantidade_de_votos > 1)
		{
			empate = true;
		}
		if(empate == false)
		{
			cout << "\nCANDIDATO A DEPUTADO DISTRITAL VENCEDOR" << endl;
			candidatos_a_deputado_distrital[indice_do_candidato_com_maior_quantidade_de_votos]-> mostrar_dados_do_candidato();
			cout << "\nQuantidade de votos: ";
			cout << candidatos_a_deputado_distrital[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() << endl;
			cout << "Porcentagem de votos: "; 
			cout << candidatos_a_deputado_distrital[indice_do_candidato_com_maior_quantidade_de_votos]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_deputado_distrital - this-> numero_de_votos_em_branco_para_deputado_distrital) << "%" << endl;
		}
		else
		{
			cout << "\nEMPATE ENTRE CANDIDATOS A DEPUTADO DISTRITAL\n";
			for(int i = 1, cont = 0; i <= indice_dos_candidatos_empatados.size(); ++i, ++cont)
			{
				cout << "\nCANDIDATO " << i << endl;
				candidatos_a_deputado_distrital[indice_dos_candidatos_empatados[cont]]-> mostrar_dados_do_candidato();
				cout << "\nQuantidade de votos: ";
				cout << candidatos_a_deputado_distrital[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() << endl;
				cout << "Porcentagem de votos: "; 
				cout << candidatos_a_deputado_distrital[indice_dos_candidatos_empatados[cont]]-> get_numero_de_votos() * 100.0 / (this-> quantidade_de_eleitores - this-> numero_de_votos_nulos_para_deputado_distrital - this-> numero_de_votos_em_branco_para_deputado_distrital) << "%" << endl;
			}
		}
	}
	cout << "\nPORCENTAGEM DE VOTOS NULOS E BRANCOS PARA DEPUTADO DISTRITAL\n";
	cout << "Votos em branco: " << (this-> numero_de_votos_em_branco_para_deputado_distrital * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "Votos nulos: " << (this-> numero_de_votos_nulos_para_deputado_distrital * 100.0) / this-> quantidade_de_eleitores << "%\n";
	cout << "\n----------------------------------------------------------------\n";
}

void Urna::mostrar_relatorio_de_votos()
{
	string linha;
	ifstream relatorio_de_votacao;	
	
	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt");

	while(!relatorio_de_votacao.eof())
	{
		getline(relatorio_de_votacao, linha);
		cout << linha << endl;
	}	
}

void Urna::limpar_arquivo_de_relatorio_de_votacao()
{
	ofstream relatorio_de_votacao;
	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt");
	relatorio_de_votacao.close();
}
