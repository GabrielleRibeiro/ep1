#ifndef SENADOR_HPP
#define SENADOR_HPP

#include "candidato.hpp"

class Senador : public Candidato
{

private:
	Candidato * primeiro_suplente;
	Candidato * segundo_suplente;

public:
	Senador();
	~Senador();

	void set_primeiro_suplente(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido);
	void set_segundo_suplente(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido);

	void mostrar_dados_do_candidato();
};

#endif
