#ifndef PRESIDENTE_HPP
#define PRESIDENTE_HPP

#include "candidato.hpp"

class Presidente : public Candidato
{

private:
	Candidato * vice_presidente;

public:
	Presidente();
	~Presidente();

	void set_vice_presidente(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido);

	void mostrar_dados_do_candidato();
};

#endif
