#include "senador.hpp"
#include <iostream>
#include <string>

using namespace std;

Senador::Senador()
{
	Candidato();
	primeiro_suplente = new Candidato();
	segundo_suplente = new Candidato();
}

Senador::~Senador()
{

}

void Senador::set_primeiro_suplente(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido)
{
	primeiro_suplente-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
}

void Senador::set_segundo_suplente(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido)
{
	segundo_suplente-> alterar_dados_do_candidato(nome, numero_do_cpf, nome_da_unidade_eleitoral, descricao_do_cargo, numero_do_candidato_na_urna, nome_de_urna_do_candidato, nome_do_partido);
}

void Senador::mostrar_dados_do_candidato()
{
	cout << endl << get_descricao_do_cargo() << " - " << get_nome_de_urna_do_candidato() << endl;
        cout << "Nome: " << get_nome() << endl;
        cout << "CPF: " << get_numero_do_cpf() << endl;
        cout << "Nome da Unidade Eleitoral: " << get_nome_da_unidade_eleitoral() << endl;
        cout << "Número do Candidato na Urna: " << get_numero_do_candidato_na_urna() << endl;
        cout << "Nome do Partido: " << get_nome_do_partido() << endl;
	primeiro_suplente-> mostrar_dados_do_candidato();
	segundo_suplente-> mostrar_dados_do_candidato();
}
