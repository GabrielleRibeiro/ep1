#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include "pessoa.hpp"
#include <string>

class Candidato : public Pessoa
{

private:
	string nome_da_unidade_eleitoral;
	string descricao_do_cargo;
	string numero_do_candidato_na_urna;
	string nome_de_urna_do_candidato;
	string nome_do_partido;
	int numero_de_votos;

public:
	Candidato();
	~Candidato();

	void set_nome_da_unidade_eleitoral(string nome_da_unidade_eleitoral);
	string get_nome_da_unidade_eleitoral();
	void set_descricao_do_cargo(string descricao_do_cargo);
	string get_descricao_do_cargo();
	void set_numero_do_candidato_na_urna(string numero_do_candidato_na_urna);
	string get_numero_do_candidato_na_urna();
	void set_nome_de_urna_do_candidato(string nome_de_urna_do_candidato);
	string get_nome_de_urna_do_candidato();
	void set_nome_do_partido(string nome_do_partido);
	string get_nome_do_partido();
	void set_numero_de_votos(int numero_de_votos);
	int get_numero_de_votos();

	void mostrar_dados_do_candidato();
	void alterar_dados_do_candidato(string nome, string numero_do_cpf, string nome_da_unidade_eleitoral, string descricao_do_cargo, string numero_do_candidato_na_urna, string nome_de_urna_do_candidato, string nome_do_partido);
	void adicionar_voto();
	void gravar_dados_em_arquivo_de_relatorio_de_votacao();

};

#endif
