#include "eleitor.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Eleitor::Eleitor()
{
	Pessoa();
	set_numero_do_titulo_de_eleitor("");
}

Eleitor::Eleitor(string nome, string numero_do_cpf, string numero_do_titulo_de_eleitor)
{
	set_nome(nome);
	set_numero_do_cpf(numero_do_cpf);
	set_numero_do_titulo_de_eleitor(numero_do_titulo_de_eleitor);
}

Eleitor::~Eleitor()
{

}

void Eleitor::set_numero_do_titulo_de_eleitor(string numero_do_titulo_de_eleitor)
{
	this-> numero_do_titulo_de_eleitor = numero_do_titulo_de_eleitor;
}

string Eleitor::get_numero_do_titulo_de_eleitor()
{
	return numero_do_titulo_de_eleitor;
}

void Eleitor::alterar_dados_do_eleitor()
{
	string nome;
	string numero_do_cpf;
	string numero_do_titulo_de_eleitor;

	cout << "\nDADOS DO ELEITOR\n";
	cout << "Informe o nome do eleitor: ";
	getchar();
	getline(cin, nome);
	cout << "Informe o cpf do eleitor: ";		
	getline(cin, numero_do_cpf);
	cout << "Informe o número do título de eleitor: ";
	getline(cin, numero_do_titulo_de_eleitor);

	set_nome(nome);
	set_numero_do_cpf(numero_do_cpf);
	set_numero_do_titulo_de_eleitor(numero_do_titulo_de_eleitor);
}

void Eleitor::gravar_dados_em_arquivo_de_relatorio_de_votacao()
{
	ofstream relatorio_de_votacao;

	relatorio_de_votacao.open("./data/Relatorio_de_votacao.txt", ios::app);

	relatorio_de_votacao << "DADOS DO ELEITOR" << endl << endl;
	relatorio_de_votacao << "Nome: " << get_nome() << endl;
	relatorio_de_votacao << "CPF: " << get_numero_do_cpf() << endl;
	relatorio_de_votacao << "Título de eleitor: " << get_numero_do_titulo_de_eleitor() << endl;

	relatorio_de_votacao.close();
}
