#include <iostream>
#include <stdlib.h>
#include "pessoa.hpp"
#include "urna.hpp"
#include "candidato.hpp"
#include "eleitor.hpp"
#include "presidente.hpp"

using namespace std;

int main()
{
	int quantidade_de_eleitores;
	int opcao_do_menu_principal;
	int opcao_do_menu_secundario;

	Urna * urna;

	do
	{
		system("clear");
		cout << "\nELEIÇÕES 2018 - URNA\n";
		cout << "\nMENU PRINCIPAL\n";
		cout << "Opções:\n";
		cout << "1 - Iniciar votação\n";
		cout << "2 - Encerrar urna\n";
		cout << "Informe o número da opção desejada: ";
		cin >> opcao_do_menu_principal;

		switch(opcao_do_menu_principal)
		{
			case 1:
				system("clear");
				cout << "\nVOTAÇÃO\n";
				cout << "Senhor(a) mesário(a), informe a quantidade de eleitores que votarão nessa urna\n";
				cout << "Quantidade de eleitores: ";
				cin >> quantidade_de_eleitores;
				
				urna = new Urna();
				urna-> registrar_candidatos();
				urna-> set_quantidade_de_eleitores(quantidade_de_eleitores);
				urna-> looping_de_votacao();

				do
				{
					system("clear");
					cout << "\nVotação encerrada!\n"; 
					cout << "Opções:\n";
					cout << "1 - Mostrar resultado da votação\n";
					cout << "2 - Mostrar relatório de votação\n";
					cout << "3 - Retornar ao menu principal\n";
					cout << "Informe o número da opção desejada: ";
					cin >> opcao_do_menu_secundario;

					switch(opcao_do_menu_secundario)
					{
						case 1:
							system("clear");
							cout << "\nRESULTADO DA VOTAÇÃO\n";
							urna-> apuracao_de_votos();
							cout << "\nOpções:\n";
							cout << "1 - Retornar\n";
							cout << "Informe a opção desejada: ";
							cin >> opcao_do_menu_secundario;
							break;	
						case 2:
							system("clear");
							cout << "\nRELATORIO DE VOTOS\n";
							cout << "O relatório de votos da votação pode ser acessado na pasta Data.\n";
							cout << "Opções:\n";
							cout << "1 - Exibir relatório de votos na tela\n";
							cout << "2 - Retornar\n";
							cout << "Informe a opção desejada: ";
							cin >> opcao_do_menu_secundario;

							if(opcao_do_menu_secundario == 1)
							{
								system("clear");
								urna-> mostrar_relatorio_de_votos();
								cout << "\nOpções:\n";
								cout << "1 - Retornar\n";
								cout << "Informe a opção desejada: ";
								cin >> opcao_do_menu_secundario;
							}
							break;
						case 3:	
							system("clear");
							cout << "\nDeseja mesmo voltar ao menu principal?\n";
							cout << "Opções:\n";
							cout << "1 - Sim\n";
							cout << "2 - Não\n";
							cout << "Informe o número da opção desejada: ";
							cin >> opcao_do_menu_secundario;

							if(opcao_do_menu_secundario == 1)
							{
								opcao_do_menu_secundario = 3;
							}
							break;
						default:
							system("clear");
							cout << "\nO número informado não corresponde a uma opção válida!\n";
							cout << "Opções:\n";
							cout << "1 - Retornar\n";
							cout << "Informe a opção desejada: ";
							cin >> opcao_do_menu_secundario;
					}
				}while(opcao_do_menu_secundario != 3);
				break;
			case 2:
				system("clear");
				cout << "\nENCERRAR URNA\n";
				cout << "Deseja mesmo encerrar a urna?\n";
				cout << "1 - Sim\n";
				cout << "2 - Não\n";
				cout << "Informe o número da opção desejada: ";
				cin >> opcao_do_menu_principal;

				if(opcao_do_menu_principal == 1)
				{
					opcao_do_menu_principal = 2;
					cout << "Urna Encerrada!\n";
					urna-> ~Urna();
				}
				else if(opcao_do_menu_principal == 2)
				{
					opcao_do_menu_principal = 1;
				}
				break;
			default:
				system("clear");
				cout << "\nO número informado não corresponde a uma opção válida!\n";
				cout << "Opções:\n";
				cout << "1 - Retornar\n";
				cout << "Informe a opção desejada: ";
				cin >> opcao_do_menu_principal;
		}
	}while(opcao_do_menu_principal != 2);

	return 0;
}


